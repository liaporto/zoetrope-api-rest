const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User', {
  email: {
    type: DataTypes.STRING,
    allowNull: false
  },
  username: {
    type: DataTypes.STRING,
    allowNull: false
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false
  },
  birthdate: {
    type: DataTypes.DATEONLY
  },
  gender: {
    type: DataTypes.STRING
  }
},{
  timestamps: false
});

// Definição das relações
User.associate = function(models){
  User.hasMany(models.Comment);
}

module.exports = User;