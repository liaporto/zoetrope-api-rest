const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Comment = sequelize.define('Comment', {
  title:{
    type: DataTypes.STRING
  },
  content: {
    type: DataTypes.STRING,
    allowNull: false
  }
});

// Definição das relações
Comment.associate = function(models){
  Comment.belongsTo(models.User);
}

module.exports = Comment;

