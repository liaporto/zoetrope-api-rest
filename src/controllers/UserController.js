const { response } = require('express');
const User = require('../models/User');

// Create
const create = async(req, res)=>{
  try{
    const user = await User.create(req.body);
    return res.status(201).json({message: "Usuário criado com sucesso!", user: user});
  }
  catch(err){
    res.status(500).json({error: err});
  }
};

// Read (All)
const index = async(req, res)=>{
  try{
    const users = await User.findAll();
    return res.status(200).json({users});
  }
  catch(err){
    return res.status(500).json({err: err});
  }
};

// Read (Specific)
const show = async(req, res)=>{
  const {id} = req.params;
  try{
    const user = await User.findByPk(id);
    return res.status(200).json({user});
  }
  catch(err){
    return res.status(500).json({err: err});
  }
};

// Update
const update = async(req,res)=>{
  const {id} = req.params;
  try{
    const [updated] = await User.update(req.body, {where: {id: id}});
    if(updated){
      const user = await User.findByPk(id);
      return res.status(201).send(user);
    }
    throw new Error();
  }
  catch(err){
    res.status(500).json("Usuário não encontrado");
  }
};

// Delete
const destroy = async(req, res)=>{
  const {id} = req.params;
  try{
    const deleted = await User.destroy({where: {id: id}});
    if(deleted){
      return res.status(200).json("Usuário deletado com sucesso.");
    }
    throw new Error();
  }
  catch{
    return res.status(500).json("Usuário não encontrado.");
  }
};

module.exports = {
  index,
  show,
  create,
  update,
  destroy
}