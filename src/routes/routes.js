const {Router} = require('express');
const UserController = require('../controllers/UserController');
const CommentController = require('../controllers/CommentController');

const router = Router();

// User routes
router.get('/users', UserController.index);
router.get('/users/:id', UserController.show);
router.post('/users', UserController.create);
router.put('/users/:id', UserController.update);
router.delete('/users/:id', UserController.destroy);

// Comment routes
router.get('/comments', CommentController.index);
router.get('/comments/:id', CommentController.show);
router.post('/comments', CommentController.create);
router.put('/comments/:id', CommentController.update);
router.delete('/comments/:id', CommentController.destroy);

// Comment-User relation
router.put('/commentsaddusers/:id', CommentController.addRelationUser);
router.delete('/commentsremoveusers/:id', CommentController.removeRelationUser);

module.exports = router;